<?php 
	$filepath = realpath(dirname(__FILE__));
	include_once ($filepath.'/../lib/Database.php');
	include_once ($filepath.'/../helpers/Format.php');
?>

<?php
	
class Customer {
	private $db;
	private $fm;
	
	public function __construct() {
		$this->db = new Database();
		$this->fm = new Format();
	}

	public function customerRegistration($data) {
		$name 		= $this->fm->validation($data['name']);
		$name 		= mysqli_real_escape_string($this->db->link, $name);

		$address	= $this->fm->validation($data['address']);
		$address 	= mysqli_real_escape_string($this->db->link, $address);

		$city 		= $this->fm->validation($data['city']);
		$city 		= mysqli_real_escape_string($this->db->link, $city);

		$country 	= $this->fm->validation($data['country']);
		$country 	= mysqli_real_escape_string($this->db->link, $country);

		$zip 		= $this->fm->validation($data['zip']);
		$zip 		= mysqli_real_escape_string($this->db->link, $zip);

		$phone 		= $this->fm->validation($data['phone']);
		$phone 		= mysqli_real_escape_string($this->db->link, $phone);

		$email 		= $this->fm->validation($data['email']);
		$email 		= mysqli_real_escape_string($this->db->link, $email);

		$pass 		= $this->fm->validation($data['pass']);
		$pass 		= mysqli_real_escape_string($this->db->link, $pass);

		// empty check
	    if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "" || $pass == "") {
	    	$msg = "<span class='error'>Fields must not be empty!</span>";
			return $msg;
	    }

	    $pass = md5($pass);

	    $mailquery = "SELECT * FROM tbl_customer WHERE email = '$email' LIMIT 1;";
	    $mailchk = $this->db->select($mailquery);
		if ($mailchk != false) {
			$msg = "<span class='error'>Email already exists!</span>";
			return $msg;
		} else {
			$query = "INSERT INTO tbl_customer(name, address, city, country, zip, phone, email, pass) VALUES('$name', '$address', '$city', '$country', '$zip', '$phone', '$email', '$pass');";
	    	$inserted_row = $this->db->insert($query);
	    	if ($inserted_row) {
				$msg = "<span class='success'>Customer data inserted successfully!</span>";
				return $msg;
			} else {
				$msg = "<span class='error'>Customer data  not inserted!</span>";
				return $msg;
			}
		}
	}

	public function customerLogin($data) {
		$email 	= $this->fm->validation($data['email']);
		$email 	= mysqli_real_escape_string($this->db->link, $email);

		$pass 	= $this->fm->validation($data['pass']);
		$pass 	= mysqli_real_escape_string($this->db->link, $pass);

		if (empty($email) || empty($pass)) {
	    	$msg = "<span class='error'>Fields must not be empty!</span>";
			return $msg;
	    } else {
	    	$pass = md5($pass);

	    	$query = "SELECT * FROM tbl_customer WHERE email = '$email' AND pass = '$pass';";
	    	$result = $this->db->select($query);
			if ($result != false) {
				$value = $result->fetch_assoc();
				Session::set("cuslogin", true);
				Session::set("cmrId", $value['id']);
				Session::set("cmrName", $value['name']);
				header("location:cart.php");
			} else {
				$msg = "<span class='error'>Email or password not matched!</span>";
				return $msg;
			}
	    }
	}

	public function getCustomerData($id) {
		$query = "SELECT * FROM tbl_customer WHERE id = '$id';";
	    $result = $this->db->select($query);
	    return $result;
	}

	public function customerUpdate($data, $cmrId) {
		$name 		= $this->fm->validation($data['name']);
		$name 		= mysqli_real_escape_string($this->db->link, $name);

		$address	= $this->fm->validation($data['address']);
		$address 	= mysqli_real_escape_string($this->db->link, $address);

		$city 		= $this->fm->validation($data['city']);
		$city 		= mysqli_real_escape_string($this->db->link, $city);

		$country 	= $this->fm->validation($data['country']);
		$country 	= mysqli_real_escape_string($this->db->link, $country);

		$zip 		= $this->fm->validation($data['zip']);
		$zip 		= mysqli_real_escape_string($this->db->link, $zip);

		$phone 		= $this->fm->validation($data['phone']);
		$phone 		= mysqli_real_escape_string($this->db->link, $phone);

		$email 		= $this->fm->validation($data['email']);
		$email 		= mysqli_real_escape_string($this->db->link, $email);

		$cmrId 		= $this->fm->validation($cmrId);
		$cmrId 		= mysqli_real_escape_string($this->db->link, $cmrId);

		// empty check
	    if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "") {
	    	$msg = "<span class='error'>Fields must not be empty!</span>";
			return $msg;
	    } else {
	    	$query = "UPDATE tbl_customer 
						SET
						name 	= '$name',
						address = '$address',
						city 	= '$city',
						country = '$country',
						zip 	= '$zip',
						phone 	= '$phone',
						email 	= '$email'
						WHERE
						id 		= '$cmrId';";
			$updated_row = $this->db->update($query);
			if ($updated_row) {
				$msg = "<span class='success'>Customer data updated successfully!</span>";
				return $msg;
			} else {
				$msg = "<span class='error'>Customer data not updated!</span>";
				return $msg;
			}
		}
	}

}

?>
