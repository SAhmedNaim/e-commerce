-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2017 at 07:40 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `adminId` int(11) NOT NULL,
  `adminName` varchar(255) NOT NULL,
  `adminUser` varchar(255) NOT NULL,
  `adminEmail` varchar(255) NOT NULL,
  `adminPass` varchar(32) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`adminId`, `adminName`, `adminUser`, `adminEmail`, `adminPass`, `level`) VALUES
(1, 'S Ahmed Naim', 'admin', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brandId` int(11) NOT NULL,
  `brandName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_brand`
--

INSERT INTO `tbl_brand` (`brandId`, `brandName`) VALUES
(1, 'Acer'),
(2, 'Samsung'),
(3, 'Iphone'),
(4, 'Canon');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cartId` int(11) NOT NULL,
  `sId` varchar(255) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `catId` int(11) NOT NULL,
  `catName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`catId`, `catName`) VALUES
(1, 'Desktop'),
(2, 'Laptop'),
(3, 'Mobile Phones'),
(4, 'Accessories'),
(5, 'Software'),
(6, 'Sports &amp; Fitness'),
(7, 'Footwear'),
(8, 'Jewellery'),
(9, 'Clothing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compare`
--

CREATE TABLE `tbl_compare` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(30) NOT NULL,
  `country` varchar(30) NOT NULL,
  `zip` varchar(30) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `name`, `address`, `city`, `country`, `zip`, `phone`, `email`, `pass`) VALUES
(1, 'S Ahmed Naim', 'West Rajabazar', 'Dhaka', 'Bangladesh', '1207', '019XXXXXXXX', 'naim.ahmed035@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `cmrId`, `productId`, `productName`, `quantity`, `price`, `image`, `date`, `status`) VALUES
(21, 1, 6, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 3, 1500.00, 'uploads/df3cff67cb.png', '2017-09-30 15:06:56', 2),
(22, 1, 4, 'Lorem ipsum', 1, 5000.00, 'uploads/2d50a17129.jpg', '2017-09-30 15:06:56', 2),
(23, 1, 8, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 1, 2000.00, 'uploads/423b1e9930.jpg', '2017-09-30 19:44:09', 2),
(24, 1, 6, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 2, 1000.00, 'uploads/df3cff67cb.png', '2017-09-30 19:44:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `catId` int(11) NOT NULL,
  `brandId` int(11) NOT NULL,
  `body` text NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`productId`, `productName`, `catId`, `brandId`, `body`, `price`, `image`, `type`) VALUES
(2, 'Lorem ipsum is simply dummy', 4, 4, '&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;', 8000.00, 'uploads/6fe771a994.png', 0),
(3, 'Lorem ipsum is simply', 3, 3, '&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;Product description will be go here...&amp;nbsp;&lt;/p&gt;', 9000.00, 'uploads/7e52be4bec.png', 1),
(4, 'Lorem ipsum', 1, 2, '&lt;p&gt;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;This is product description. . .&amp;nbsp;&lt;/p&gt;', 5000.00, 'uploads/2d50a17129.jpg', 0),
(5, 'Table Fan', 4, 3, '&lt;p&gt;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;This is product description...&amp;nbsp;&lt;/p&gt;', 4000.00, 'uploads/4622b840c8.jpg', 1),
(6, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 3, 3, '&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;', 500.00, 'uploads/df3cff67cb.png', 0),
(7, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 4, 2, '&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;', 2000.00, 'uploads/8aab711a5b.png', 1),
(8, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 4, 1, '&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;', 2000.00, 'uploads/423b1e9930.jpg', 0),
(9, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 6, 4, '&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;Lorem ipsum dolor sit amet, sed do eiusmod.&amp;nbsp;&lt;/p&gt;', 4000.00, 'uploads/e875742e70.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wlist`
--

CREATE TABLE `tbl_wlist` (
  `id` int(11) NOT NULL,
  `cmrId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wlist`
--

INSERT INTO `tbl_wlist` (`id`, `cmrId`, `productId`, `productName`, `price`, `image`) VALUES
(5, 1, 6, 'Lorem ipsum dolor sit amet, sed do eiusmod.', 500.00, 'uploads/df3cff67cb.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brandId`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cartId`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`catId`);

--
-- Indexes for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`productId`);

--
-- Indexes for table `tbl_wlist`
--
ALTER TABLE `tbl_wlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brandId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cartId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_compare`
--
ALTER TABLE `tbl_compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_wlist`
--
ALTER TABLE `tbl_wlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
